IHC Mercs is a collection of class specific macro's that use a unified command structure.
Meant to be an alternative to the all-in-one macs and simulate hiring a mercenary in game.

Nothing beats the ease of hiring a merc in game, but lets face it...they suck and offer barely any customization!
All-in-one mac's are awsome and offer the most flexability usually but they require quite a bit of setup and can get slow pouring over every possible option for every class and situation.
Class specific macs are usually fast and more like a player, flexability/customization varies and different commands to start each one gets annoying as does different commands to do the same thing in each mac.
Plugins I find to be much like a class mac but faster with the added bonus of being able to run a mac at the same time. Downside is fixes/additions tend to be slow and if you like to really customize and play with code its not happening here unless you have access to the source.

Why should I use these instead of xxx?
 They all come with a default configuration for every level 1-Max that will preform their general role 100x better than any in game merc ( and probably your average player ) with 0 setup time. Add the class to your group, give it the same start command as the rest of your bots and go!
 Fully customizable! These mac's can be tuned for all situations and ungodly speed, most with simple on/off switches. MOST options can be set from the command line, make adjustments on the fly and see results in realtime, no need to stop the mac, edit the file and restart ( you can do this also if you like )
 Not all of these mac's are mine, but I have modded all to add more abilities, config options and commands.

Current Mac's:
IHCBER - Berserker
IHCBST - Beastlord - Toot's autobeast with Expanded level range use, specific modes ( support healer, pet tank, melee only, caster only, balanced ), adapted to ihcsubs and expanded customization
IHCCLR - Cleric - Noobhaxor's auto_CLR with Expanded curing, different logic for pet healing/pet tank healing, adapted to ihcsubs and expanded customization.
IHCENC - Enchanter
IHCWAR- Warrior - Noobhaxor's auto_WAR Expanded combat logic, adapted to ihcsubs and expanded customization
IHCWIZ - Wizard - Based off my kiss ini, meant for high sustained dps without running oom.
ihcsubs.inc - Modified and expanded autosubs

Installation:
 Unzip into your macro's folder, this will add ihcstart.mac and create a new folder named IHCMercs. The IHCMercs directory is where all your characters ini files will be created.
 
Use:
 *first time use* load each class individually /mac ihcstart then /end
 Check the ini file and make any adjustments you might want to make, most have an out of the box config to preform their general role, ex: IHCENC is going to do full debuffing tash, slow, cripple, strip buffs...this might be a waste of time for your group or content, if mobs are dead before they are slowed or crippled...turn it off!
 After the initial load and adjustments **target your main assist/tank and start the mac with /ihcstart on each char or all at once using bc....ex: /bcga //ihstart will start the mac on all supported classes.
 
 **Main assist must make sense, it is enforced by certain mac's....ex: the enchanter cant be MA because some of the roles like CC...if the ench is the MA and goes to CC a target everyone would jump on it!
 
Tuning and Gamaplay Tips:
==========================
High Mana Use:
	Try using bring your own spells ( /byos toggles it ). Look at whats spells are memorized and try unmeming the highest mana use spells 1 at a time, replace the gem with a buff spell.

Keeping Spell sets up to date ( byos users ):
	Each time you buy new spells, if you have /byos on, toggle it off, let it mem new spells, /byos back on and adjust as necessary

