|- ihcbrdutil.inc
|- Version 0.1
|- Updated: 5/24/2019
|--------------------------------------------------------------------------------------
|-Setup Variable Environment
|--------------------------------------------------------------------------------------
Sub BrdSetup
|-Create Alias
	|-- Toggles
	/squelch /alias /byos			/setvarint Toggle BringYourOwnSpells
	/squelch /alias /tglaoe			/setvarint Toggle UseAoE
	/squelch /alias /tglbp			/setvarint Toggle ChestCombatCast
	/squelch /alias /tglpet			/setvarint Toggle UsePet
	/squelch /alias /tglpoison		/setvarint Toggle UsePoisons
	/squelch /alias /tgldots		/setvarint Toggle DoDot
	/squelch /alias /tglmezbreak	/setvarint Toggle AllowMezBreak
	/squelch /alias /tglslow		/setvarint Toggle DoSlow
	/squelch /alias /tglselo		/setvarint Toggle UseAASelo
	/squelch /alias /tglmanasong	/setvarint Toggle OOCManaSong
	/squelch /alias /tglaura		/setvarint Toggle UseAura
|-Spells
	/declare BardAura				string outer NULL
	/declare BardDPSAura			string outer NULL
	/declare BardRegenAura			string outer NULL
	/declare PulseRegenSong			string outer NULL
	/declare ChorusRegenSong		string outer NULL
	/declare AEMezSong				string outer NULL
	/declare MezSong				string outer NULL
	/declare DichoSong				string outer NULL
	/declare MainAriaSong			string outer NULL
	/declare CasterAriaSong			string outer NULL
	/declare WarMarchSong			string outer NULL
	/declare SufferingSong			string outer NULL
	/declare ArcaneSong				string outer NULL
	/declare SprySonataSong			string outer NULL
	/declare CrescendoSong			string outer NULL
	/declare InsultSong1			string outer NULL
	/declare InsultSong2			string outer NULL
	/declare SlowSong				string outer NULL
	/declare AESlowSong				string outer NULL
	/declare AccelerandoSong		string outer NULL
	/declare SpitefulSong			string outer NULL
	/declare RecklessSong			string outer NULL
|-Special
	/declare Epicsword				string outer NULL
	/declare UseEpic 				int outer 1
|- Misc Variables
	/declare Song1					string outer NULL
	/declare Song2					string outer NULL
	/declare Song3					string outer NULL
	/declare Song4					string outer NULL
	/declare Song5					string outer NULL
	/declare Song6					string outer NULL
	/declare Song7					string outer NULL
	/declare Song8					string outer NULL
	/declare Song9					string outer NULL
	/declare Song10					string outer NULL
	/declare Song11					string outer NULL
	/declare Song12					string outer NULL
	/declare Song13					string outer NULL
	/declare BattleMelody			string outer 1 2 3 4 1 5 2 3 1 4 5 2 1 3 4 5 7 1 2 10
	/declare ExtBattleMelody		string outer 1 2 3 4 5 6 1 2 3 4 5 6 1 2 7 3 4 5 1 10
	/declare BurnMelody				string outer 8 9 1 8 9 2 8 9 1 8 9 3
	/declare ExtBurnMelody			string outer 8 1 2 8 3 4 8 1 5 8 2 3 8 1 4 8 5 2 8 1 10
	/declare MelodyType				string outer "Valid Settings are Melee (Default), Caster, Tank"
	/declare CurrentMelody			string outer NULL
	/declare MezAETimer             timer	outer	0
|-Config Options
	/call LoadCommonConfig
	/call LoadIni Combat AllowMezBreak		 	int 0
	/call LoadIni Combat DoSlow					int 0
	/call LoadIni Combat StickHow				string behind
	/call LoadIni Combat UseMelee		 		int 1
	/call LoadIni Combat UseIntimidate		 	int 1
	/call LoadIni Combat MelodyType				String ${MelodyType}
	/call LoadIni Combat UseCustomMelody		int 0
	/call LoadIni Combat CustomMelody			String NULL
	/call LoadIni Item ChestItemName			string NULL
	/call LoadIni Item ChestCombatCast			int 0
	/call LoadIni Item UsePoisons		 		int 0
	/call LoadIni Item PoisonName				string NULL
	/call LoadIni Item PoisonBuffName			string NULL
	/call LoadIni Mez MezOn                 	int 1
	/call LoadIni Mez MezAnnounce               int 1	
	/call LoadIni Mez MezStartCount             int 2
	/call LoadIni Options BringYourOwnSpells	int 0
	/call LoadIni Options OOCManaSong			int 1
	/call LoadIni Options UseAura				int 1
	/call LoadIni Options UseRegenAura			int 0
	/call LoadIni Options UseAASelo				int 1
	/call LoadIni Options UseIniSongs			int 0
	/call LoadIni Pet UseSwarmpet		 		int 0
|- Check for epic
	/if (${FindItem[=Prismatic Dragon Blade].ID}) {
		/varset Epicsword	Prismatic Dragon Blade	
		/varset UseEpic 1
	} else /if (${FindItem[=Blade of Vesagran].ID}) {
		/varset Epicsword	Blade of Vesagran
		/varset UseEpic 1
	}
|-Declare spell variables
	/declare spellmisc 			int outer 8
	/declare GroupBuffRecast 	int outer 0
	/declare miscspellremem		string outer NULL
	/declare CastResult         string outer
	/declare spellrememtimer	timer outer 0
	/declare groupbufftimer		timer outer 0
	/declare ReadyToCast	  	timer outer 0
	/if (${Me.AltAbility[Mnemonic Retention].Rank}==5) /varset spellmisc 13
	/if (${Me.AltAbility[Mnemonic Retention].Rank}==4) /varset spellmisc 12
	/if (${Me.AltAbility[Mnemonic Retention].Rank}==3) /varset spellmisc 11
	/if (${Me.AltAbility[Mnemonic Retention].Rank}==2) /varset spellmisc 10
	/if (${Me.AltAbility[Mnemonic Retention].Rank}==1) /varset spellmisc 9
|-	/if (${Me.AltAbility[8300].Name.Find[disabled]} && ${Me.AltAbilityReady[8300]}) {
|-		/alt act 8300
|-		/delay 5
|-	}
/return 
|----------------------------------------------------------------------------
|- SUB: INIChanges
|---------------------------------------------------------------------------- 
Sub INIChanges
	/varset changetoini 0
	/call SaveCommonConfig
	/call SetIni Combat AllowMezBreak		 	int ${AllowMezBreak}
	/call SetIni Combat DoSlow					int ${DoSlow}
	/call SetIni Combat UseMelee		 		int ${UseMelee}
	/call SetIni Item ChestCombatCast			int ${ChestCombatCast}
	/call SetIni Item UsePoisons		 		int ${UsePoisons}
	/call SetIni Options BringYourOwnSpells		int ${BringYourOwnSpells}
	/call SetIni Pet UseSwarmpet		 		int ${UseSwarmpet}
/return
|* ------------------------------------------------------------------------------------------
| SUB: LoadSpellBar
|------------------------------------------------------------------------------------------ *|
Sub LoadSpellBar
	/if (!${BringYourOwnSpells}) {
		/if (${Me.Standing}) /sit
		/delay 10 ${Me.Sitting}
		/echo \aw Loading Spell Bar	
		/call LoadSpellGem "${Song1}" 1
		/call LoadSpellGem "${Song2}" 2
		/call LoadSpellGem "${Song3}" 3
		/call LoadSpellGem "${Song4}" 4
		/call LoadSpellGem "${Song5}" 5
		/call LoadSpellGem "${Song6}" 6
		/call LoadSpellGem "${Song7}" 7
		/call LoadSpellGem "${Song8}" 8
		/if (${spellmisc}>8) {
			/call LoadSpellGem "${Song9}" 9
			/varset miscspellremem ${Song9}
		}
		/if (${spellmisc}>9) {
			/call LoadSpellGem "${Song10}" 10
			/varset miscspellremem ${Song10}
		}
		/if (${spellmisc}>10) {
			/call LoadSpellGem "${Song11}" 11
		}
		/if (${spellmisc}>11) {
			/call LoadSpellGem "${Song12}" 12
			/varset miscspellremem ${Song12}
		}
		/if (${spellmisc}>12) {
			/call LoadSpellGem "${Song13}" 13
		}
	}
	/if (${Me.Sitting}) /stand
	/delay 10 ${Me.Standing}
/return 
|----------------------------------------------------------------------------
|- SUB: CheckSpells
|---------------------------------------------------------------------------- 
Sub CheckSpells
|- BardDPSAura
	/if (${Me.Level}>=110 && ${Me.Book[${Spell[Aura of Begalru].RankName}]})  {
		/varset BardDPSAura ${Spell[Aura of Begalru].RankName}
	} else /if (${Me.Level}>=105 && ${Me.Book[${Spell[Aura of Maetanrus].RankName}]})  {
		/varset BardDPSAura ${Spell[Aura of Maetanrus].RankName}
	} else /if (${Me.Level}>=100 && ${Me.Book[${Spell[Aura of Va'Ker].RankName}]})  {
		/varset BardDPSAura ${Spell[Aura of Va'Ker].RankName}
	} else /if (${Me.Level}>=95 && ${Me.Book[${Spell[Aura of the Orator].RankName}]})  {
		/varset BardDPSAura ${Spell[Aura of the Orator].RankName}
	} else /if (${Me.Level}>=90 && ${Me.Book[${Spell[Aura of the Composer].RankName}]})  {
		/varset BardDPSAura ${Spell[Aura of the Composer].RankName}
	} else /if (${Me.Level}>=85 && ${Me.Book[${Spell[Aura of the Poet].RankName}]})  {
		/varset BardDPSAura ${Spell[Aura of the Poet].RankName}
	}
|- BardRegenAura
	/if (${Me.Level}>=107 && ${Me.Book[${Spell[Aura of Xigam].RankName}]})  {
		/varset BardRegenAura ${Spell[Aura of Xigam].RankName}
	} else /if (${Me.Level}>=102 && ${Me.Book[${Spell[Aura of Sionachie].RankName}]})  {
		/varset BardRegenAura ${Spell[Aura of Sionachie].RankName}
	} else /if (${Me.Level}>=97 && ${Me.Book[${Spell[Aura of Salarra].RankName}]})  {
		/varset BardRegenAura ${Spell[Aura of Salarra].RankName}
	} else /if (${Me.Level}>=92 && ${Me.Book[${Spell[Aura of Lunanyn].RankName}]})  {
		/varset BardRegenAura ${Spell[Aura of Lunanyn].RankName}
	} else /if (${Me.Level}>=87 && ${Me.Book[${Spell[Aura of Renewal].RankName}]})  {
		/varset BardRegenAura ${Spell[Aura of Renewal].RankName}
	}
	/call SetupAura
|- MainAriaSong
	/if (${Me.Level}>=106 && ${Me.Book[${Spell[Aria of Begalru].RankName}]})  {
		/varset MainAriaSong ${Spell[Aria of Begalru].RankName}
	} else /if (${Me.Level}>=101 && ${Me.Book[${Spell[Aria of Maetanrus].RankName}]})  {
		/varset MainAriaSong ${Spell[Aria of Maetanrus].RankName}
	} else /if (${Me.Level}>=96 && ${Me.Book[${Spell[Aria of Va'Ker].RankName}]})  {
		/varset MainAriaSong ${Spell[Aria of Va'Ker].RankName}
	} else /if (${Me.Level}>=91 && ${Me.Book[${Spell[Aria of the Orator].RankName}]})  {
		/varset MainAriaSong ${Spell[Aria of the Orator].RankName}
	} else /if (${Me.Level}>=86 && ${Me.Book[${Spell[Aria of the Composer].RankName}]})  {
		/varset MainAriaSong ${Spell[Aria of the Composer].RankName}
	}
|- CasterAriaSong
	/if (${Me.Level}>=108 && ${Me.Book[${Spell[Qunard's Aria].RankName}]})  {
		/varset CasterAriaSong ${Spell[Qunard's Aria].RankName}
	} else /if (${Me.Level}>=103 && ${Me.Book[${Spell[Nilsara's Aria].RankName}]})  {
		/varset CasterAriaSong ${Spell[Nilsara's Aria].RankName}
	} else /if (${Me.Level}>=98 && ${Me.Book[${Spell[Gosik's Aria].RankName}]})  {
		/varset CasterAriaSong ${Spell[Gosik's Aria].RankName}
	} else /if (${Me.Level}>=93 && ${Me.Book[${Spell[Daevan's Aria].RankName}]})  {
		/varset CasterAriaSong ${Spell[Daevan's Aria].RankName}
	} else /if (${Me.Level}>=88 && ${Me.Book[${Spell[Sotor's Aria].RankName}]})  {
		/varset CasterAriaSong ${Spell[Sotor's Aria].RankName}
	}
|- WarMarchSong
	/if (${Me.Level}>=109 && ${Me.Book[${Spell[War March of Dekloaz].RankName}]})  {
		/varset WarMarchSong ${Spell[War March of Dekloaz].RankName}
	} else /if (${Me.Level}>=104 && ${Me.Book[${Spell[War March of Jocelyn].RankName}]})  {
		/varset WarMarchSong ${Spell[War March of Jocelyn].RankName}
	} else /if (${Me.Level}>=99 && ${Me.Book[${Spell[War March of Protan].RankName}]})  {
		/varset WarMarchSong ${Spell[War March of Protan].RankName}
	} else /if (${Me.Level}>=94 && ${Me.Book[${Spell[War March of Illdaera].RankName}]})  {
		/varset WarMarchSong ${Spell[War March of Illdaera].RankName}
	} else /if (${Me.Level}>=89 && ${Me.Book[${Spell[War March of Dagda].RankName}]})  {
		/varset WarMarchSong ${Spell[War March of Dagda].RankName}
	} else /if (${Me.Level}>=84 && ${Me.Book[${Spell[War March of Brekt].RankName}]})  {
		/varset WarMarchSong ${Spell[War March of Brekt].RankName}
	} 
|- SufferingSong
	/if (${Me.Level}>=109 && ${Me.Book[${Spell[Travenro's Song of Suffering].RankName}]})  {
		/varset SufferingSong ${Spell[Travenro's Song of Suffering].RankName}
	} else /if (${Me.Level}>=104 && ${Me.Book[${Spell[Fjilnauk's Song of Suffering].RankName}]})  {
		/varset SufferingSong ${Spell[Fjilnauk's Song of Suffering].RankName}
	} else /if (${Me.Level}>=99 && ${Me.Book[${Spell[Kaficus' Song of Suffering].RankName}]})  {
		/varset SufferingSong ${Spell[Kaficus' Song of Suffering].RankName}
	} else /if (${Me.Level}>=94 && ${Me.Book[${Spell[Hykast's Song of Suffering].RankName}]})  {
		/varset SufferingSong ${Spell[Hykast's Song of Suffering].RankName}
	} else /if (${Me.Level}>=89 && ${Me.Book[${Spell[Noira's Song of Suffering].RankName}]})  {
		/varset SufferingSong ${Spell[Noira's Song of Suffering].RankName}
	} 
|- ArcaneSong
	/if (${Me.Level}>=110 && ${Me.Book[${Spell[Arcane Ballad].RankName}]})  {
		/varset ArcaneSong ${Spell[Arcane Ballad].RankName}
	} else /if (${Me.Level}>=105 && ${Me.Book[${Spell[Arcane Melody].RankName}]})  {
		/varset ArcaneSong ${Spell[Arcane Melody].RankName}
	} else /if (${Me.Level}>=100 && ${Me.Book[${Spell[Arcane Hymn].RankName}]})  {
		/varset ArcaneSong ${Spell[Arcane Hymn].RankName}
	} else /if (${Me.Level}>=95 && ${Me.Book[${Spell[Arcane Address].RankName}]})  {
		/varset ArcaneSong ${Spell[Arcane Address].RankName}
	} else /if (${Me.Level}>=90 && ${Me.Book[${Spell[Arcane Chorus].RankName}]})  {
		/varset ArcaneSong ${Spell[Arcane Chorus].RankName}
	} else /if (${Me.Level}>=85 && ${Me.Book[${Spell[Arcane Arietta].RankName}]})  {
		/varset ArcaneSong ${Spell[Arcane Arietta].RankName}
	}
|- SprySonataSong
	/if (${Me.Level}>=108 && ${Me.Book[${Spell[Kluzen's Spry Sonata].RankName}]})  {
		/varset SprySonataSong ${Spell[Kluzen's Spry Sonata].RankName}
	} else /if (${Me.Level}>=98 && ${Me.Book[${Spell[Doben's Spry Sonata].RankName}]})  {
		/varset SprySonataSong ${Spell[Doben's Spry Sonata].RankName}
	} else /if (${Me.Level}>=93 && ${Me.Book[${Spell[Terasal's Spry Sonata].RankName}]})  {
		/varset SprySonataSong ${Spell[Terasal's Spry Sonata].RankName}
	} else /if (${Me.Level}>=88 && ${Me.Book[${Spell[Sionachie's Spry Sonata].RankName}]})  {
		/varset SprySonataSong ${Spell[Sionachie's Spry Sonata].RankName}
	}
|- PulseRegenSong
	/if (${Me.Level}>=106 && ${Me.Book[${Spell[Pulse of Xigam].RankName}]})  {
		/varset PulseRegenSong ${Spell[Pulse of Xigam].RankName}
	} else /if (${Me.Level}>=101 && ${Me.Book[${Spell[Pulse of Sionachie].RankName}]})  {
		/varset PulseRegenSong ${Spell[Pulse of Sionachie].RankName}
	} else /if (${Me.Level}>=96 && ${Me.Book[${Spell[Pulse of Salarra].RankName}]})  {
		/varset PulseRegenSong ${Spell[Pulse of Salarra].RankName}
	} else /if (${Me.Level}>=91 && ${Me.Book[${Spell[Pulse of Lunanyn].RankName}]})  {
		/varset PulseRegenSong ${Spell[Pulse of Lunanyn].RankName}
	} else /if (${Me.Level}>=86 && ${Me.Book[${Spell[Pulse of Renewal].RankName}]})  {
		/varset PulseRegenSong ${Spell[Pulse of Renewal].RankName}
	} else /if (${Me.Level}>=82 && ${Me.Book[${Spell[Pulse of Rodcet].RankName}]})  {
		/varset PulseRegenSong ${Spell[Pulse of Rodcet].RankName}
	} else /if (${Me.Level}>=77 && ${Me.Book[${Spell[Rhythm of Restoration].RankName}]})  {
		/varset PulseRegenSong ${Spell[Rhythm of Restoration].RankName}
	}
|- ChorusRegenSong
	/if (${Me.Level}>=108 && ${Me.Book[${Spell[Chorus of Xigam].RankName}]})  {
		/varset ChorusRegenSong ${Spell[Chorus of Xigam].RankName}
	} else /if (${Me.Level}>=103 && ${Me.Book[${Spell[Chorus of Sionachie].RankName}]})  {
		/varset ChorusRegenSong ${Spell[Chorus of Sionachie].RankName}
	} else /if (${Me.Level}>=98 && ${Me.Book[${Spell[Chorus of Salarra].RankName}]})  {
		/varset ChorusRegenSong ${Spell[Chorus of Salarra].RankName}
	} else /if (${Me.Level}>=93 && ${Me.Book[${Spell[Chorus of Lunanyn].RankName}]})  {
		/varset ChorusRegenSong ${Spell[Chorus of Lunanyn].RankName}
	} else /if (${Me.Level}>=88 && ${Me.Book[${Spell[Chorus of Renewal].RankName}]})  {
		/varset ChorusRegenSong ${Spell[Chorus of Renewal].RankName}
	}
|- CrescendoSong
	/if (${Me.Level}>=109 && ${Me.Book[${Spell[Jembel's Lively Crescendo].RankName}]})  {
		/varset CrescendoSong ${Spell[Jembel's Lively Crescendo].RankName}
	} else /if (${Me.Level}>=104 && ${Me.Book[${Spell[Silisia's Lively Crescendo].RankName}]})  {
		/varset CrescendoSong ${Spell[Silisia's Lively Crescendo].RankName}
	} else /if (${Me.Level}>=100 && ${Me.Book[${Spell[Motlak's Lively Crescendo].RankName}]})  {
		/varset CrescendoSong ${Spell[Motlak's Lively Crescendo].RankName}
	} else /if (${Me.Level}>=95 && ${Me.Book[${Spell[Kolain's Lively Crescendo].RankName}]})  {
		/varset CrescendoSong ${Spell[Kolain's Lively Crescendo].RankName}
	} else /if (${Me.Level}>=90 && ${Me.Book[${Spell[Lyssa's Lively Crescendo].RankName}]})  {
		/varset CrescendoSong ${Spell[Lyssa's Lively Crescendo].RankName}
	} else /if (${Me.Level}>=85 && ${Me.Book[${Spell[Gruber's Lively Crescendo].RankName}]})  {
		/varset CrescendoSong ${Spell[Gruber's Lively Crescendo].RankName}
	} 
|- InsultSong1
	/if (${Me.Level}>=110 && ${Me.Book[${Spell[Sathir's Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Sathir's Insult].RankName}
	} else /if (${Me.Level}>=107 && ${Me.Book[${Spell[Travenro's Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Travenro's Insult].RankName}
	} else /if (${Me.Level}>=105 && ${Me.Book[${Spell[Tsaph's Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Tsaph's Insult].RankName}
	} else /if (${Me.Level}>=102 && ${Me.Book[${Spell[Fjilnauk's Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Fjilnauk's Insult].RankName}
	} else /if (${Me.Level}>=100 && ${Me.Book[${Spell[Kaficus' Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Kaficus' Insult].RankName}
	} else /if (${Me.Level}>=97 && ${Me.Book[${Spell[Garath's Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Garath's Insult].RankName}
	} else /if (${Me.Level}>=95 && ${Me.Book[${Spell[Hykast's Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Hykast's Insult].RankName}
	} else /if (${Me.Level}>=90 && ${Me.Book[${Spell[Lyrin's Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Lyrin's Insult].RankName}
	} else /if (${Me.Level}>=85 && ${Me.Book[${Spell[Venimor's Insult].RankName}]})  {
		/varset InsultSong1 ${Spell[Venimor's Insult].RankName}
	}
|- InsultSong2
	/if (${Me.Level}>=110 && ${Me.Book[${Spell[Travenro's Insult].RankName}]})  {
		/varset InsultSong2 ${Spell[Travenro's Insult].RankName}
	} else /if (${Me.Level}>=107 && ${Me.Book[${Spell[Tsaph's Insult].RankName}]})  {
		/varset InsultSong2 ${Spell[Tsaph's Insult].RankName}
	} else /if (${Me.Level}>=105 && ${Me.Book[${Spell[Fjilnauk's Insult].RankName}]})  {
		/varset InsultSong2 ${Spell[Fjilnauk's Insult].RankName}
	} else /if (${Me.Level}>=102 && ${Me.Book[${Spell[Kaficus' Insult].RankName}]})  {
		/varset InsultSong2 ${Spell[Kaficus' Insult].RankName}
	} else /if (${Me.Level}>=100 && ${Me.Book[${Spell[Garath's Insult].RankName}]})  {
		/varset InsultSong2 ${Spell[Garath's Insult].RankName}
	} else /if (${Me.Level}>=97 && ${Me.Book[${Spell[Hykast's Insult].RankName}]})  {
		/varset InsultSong2 ${Spell[Hykast's Insult].RankName}
	} else /if (${Me.Level}>=95 && ${Me.Book[${Spell[Lyrin's Insult].RankName}]})  {
		/varset InsultSong2 ${Spell[Lyrin's Insult].RankName}
	} else /if (${Me.Level}>=90 && ${Me.Book[${Spell[Venimor's Insult].RankName}]})  {
		/varset InsultSong2 ${Spell[Venimor's Insult].RankName}
	}
|- DichoSong
	/if (${Me.Level}>=106 && ${Me.Book[${Spell[Dissident Psalm].RankName}]})  {
		/varset DichoSong ${Spell[Dissident Psalm].RankName}
	} else /if (${Me.Level}>=101 && ${Me.Book[${Spell[Dichotomic Psalm].RankName}]})  {
		/varset DichoSong ${Spell[Dichotomic Psalm].RankName}
	}
|- MezSong
	/if (${Me.Level}>=109 && ${Me.Book[${Spell[Slumber of Jembel].RankName}]})  {
		/varset MezSong ${Spell[Slumber of Jembel].RankName}
	} else /if (${Me.Level}>=106 && ${Me.Book[${Spell[Lullaby of Jembel].RankName}]})  {
		/varset MezSong ${Spell[Lullaby of Jembel].RankName}
	} else /if (${Me.Level}>=104 && ${Me.Book[${Spell[Slumber of Silisia].RankName}]})  {
		/varset MezSong ${Spell[Slumber of Silisia].RankName}
	} else /if (${Me.Level}>=101 && ${Me.Book[${Spell[Lullaby of Silisia].RankName}]})  {
		/varset MezSong ${Spell[Lullaby of Silisia].RankName}
	} else /if (${Me.Level}>=99 && ${Me.Book[${Spell[Slumber of Motlak].RankName}]})  {
		/varset MezSong ${Spell[Slumber of Motlak].RankName}
	} else /if (${Me.Level}>=96 && ${Me.Book[${Spell[Lullaby of the Forsaken].RankName}]})  {
		/varset MezSong ${Spell[Lullaby of the Forsaken].RankName}
	} else /if (${Me.Level}>=94 && ${Me.Book[${Spell[Slumber of Kolain].RankName}]})  {
		/varset MezSong ${Spell[Slumber of Kolain].RankName}
	} else /if (${Me.Level}>=91 && ${Me.Book[${Spell[Lullaby of the Forlorn].RankName}]})  {
		/varset MezSong ${Spell[Lullaby of the Forlorn].RankName}
	} else /if (${Me.Level}>=89 && ${Me.Book[${Spell[Slumber of Sionachie].RankName}]})  {
		/varset MezSong ${Spell[Slumber of Sionachie].RankName}
	} else /if (${Me.Level}>=86 && ${Me.Book[${Spell[Lullaby of the Lost].RankName}]})  {
		/varset MezSong ${Spell[Lullaby of the Lost].RankName}
	}
|- AEMezSong
	/if (${Me.Level}>=110 && ${Me.Book[${Spell[Wave of Somnolence].RankName}]})  {
		/varset AEMezSong ${Spell[Wave of Somnolence].RankName}
	} else /if (${Me.Level}>=105 && ${Me.Book[${Spell[Wave of Torpor].RankName}]})  {
		/varset AEMezSong ${Spell[Wave of Torpor].RankName}
	} else /if (${Me.Level}>=100 && ${Me.Book[${Spell[Wave of Quietude].RankName}]})  {
		/varset AEMezSong ${Spell[Wave of Quietude].RankName}
	} else /if (${Me.Level}>=95 && ${Me.Book[${Spell[Wave of the Conductor].RankName}]})  {
		/varset AEMezSong ${Spell[Wave of the Conductor].RankName}
	} else /if (${Me.Level}>=90 && ${Me.Book[${Spell[Wave of Dreams].RankName}]})  {
		/varset AEMezSong ${Spell[Wave of Dreams].RankName}
	} else /if (${Me.Level}>=85 && ${Me.Book[${Spell[Wave of Slumber].RankName}]})  {
		/varset AEMezSong ${Spell[Wave of Slumber].RankName}
	} 
|- SlowSong
	/if (${Me.Level}>=64 && ${Me.Book[${Spell[Requiem of Time].RankName}]})  {
		/varset SlowSong ${Spell[Requiem of Time].RankName}
	} 
|- AESlowSong
	/if (${Me.Level}>=109 && ${Me.Book[${Spell[Dekloaz's Melodic Binding].RankName}]})  {
		/varset AESlowSong ${Spell[Dekloaz's Melodic Binding].RankName}
	} else /if (${Me.Level}>=99 && ${Me.Book[${Spell[Protan's Melodic Binding].RankName}]})  {
		/varset AESlowSong ${Spell[Protan's Melodic Binding].RankName}
	}
 |- AccelerandoSong
 	/if (${Me.Level}>=108 && ${Me.Book[${Spell[Atoning Accelerando].RankName}]})  {
		/varset AccelerandoSong ${Spell[Atoning Accelerando].RankName}
	} else /if (${Me.Level}>=103 && ${Me.Book[${Spell[Allaying Accelerando].RankName}]})  {
		/varset AccelerandoSong ${Spell[Allaying Accelerando].RankName}
	} else /if (${Me.Level}>=98 && ${Me.Book[${Spell[Ameliorating Accelerando].RankName}]})  {
		/varset AccelerandoSong ${Spell[Ameliorating Accelerando].RankName}
	} else /if (${Me.Level}>=93 && ${Me.Book[${Spell[Assuaging Accelerando].RankName}]})  {
		/varset AccelerandoSong ${Spell[Assuaging Accelerando].RankName}
	} else /if (${Me.Level}>=88 && ${Me.Book[${Spell[Alleviating Accelerando].RankName}]})  {
		/varset AccelerandoSong ${Spell[Alleviating Accelerando].RankName}
	}
 |- SpitefulSong
 	/if (${Me.Level}>=110 && ${Me.Book[${Spell[Travenro's Spiteful Lyric].RankName}]})  {
		/varset SpitefulSong ${Spell[Travenro's Spiteful Lyric].RankName}
	} else /if (${Me.Level}>=105 && ${Me.Book[${Spell[Fjilnauk's Spiteful Lyric].RankName}]})  {
		/varset SpitefulSong ${Spell[Fjilnauk's Spiteful Lyric].RankName}
	} else /if (${Me.Level}>=100 && ${Me.Book[${Spell[Kaficus' Spiteful Lyric].RankName}]})  {
		/varset SpitefulSong ${Spell[Kaficus' Spiteful Lyric].RankName}
	} else /if (${Me.Level}>=95 && ${Me.Book[${Spell[Hykast's Spiteful Lyric].RankName}]})  {
		/varset SpitefulSong ${Spell[Hykast's Spiteful Lyric].RankName}
	} else /if (${Me.Level}>=90 && ${Me.Book[${Spell[Lyrin's Spiteful Lyric].RankName}]})  {
		/varset SpitefulSong ${Spell[Lyrin's Spiteful Lyric].RankName}
	}
 |- RecklessSong
 	/if (${Me.Level}>=108 && ${Me.Book[${Spell[Rigelon's Reckless Renewal].RankName}]})  {
		/varset RecklessSong ${Spell[Rigelon's Reckless Renewal].RankName}
	} else /if (${Me.Level}>=103 && ${Me.Book[${Spell[Rytan's Reckless Renewal].RankName}]})  {
		/varset RecklessSong ${Spell[Rytan's Reckless Renewal].RankName}
	} else /if (${Me.Level}>=98 && ${Me.Book[${Spell[Ruaabri's Reckless Renewal].RankName}]})  {
		/varset RecklessSong ${Spell[Ruaabri's Reckless Renewal].RankName}
	} else /if (${Me.Level}>=93 && ${Me.Book[${Spell[Ryken's Reckless Renewal].RankName}]})  {
		/varset RecklessSong ${Spell[Ryken's Reckless Renewal].RankName}
	}
	
	/if (${MelodyType.Equal[Caster]}) {
		/varset Song1 ${MainAriaSong}
		/varset Song2 ${ArcaneSong}
		/varset Song3 ${SufferingSong}
		/varset Song4 ${WarMarchSong}
		/varset Song5 ${CasterAriaSong}
		/varset Song6 ${SprySonataSong}
		/varset Song7 ${CrescendoSong}
		/varset Song8 ${InsultSong1}
		/varset Song9 ${InsultSong2}
		/varset Song10 ${DichoSong}
		/varset Song11 ${AEMezSong}
		/varset Song12 ${ChorusRegenSong}
		/varset Song13 ${MezSong}
	} else /if (${MelodyType.Equal[Tank]}) {
		/varset Song1 ${AccelerandoSong}
		/varset Song2 ${SprySonataSong}
		/varset Song3 ${SpitefulSong}
		/varset Song4 ${PulseRegenSong}
		/varset Song5 ${RecklessSong}
		/varset Song6 ${ChorusRegenSong}
		/varset Song7 ${CrescendoSong}
		/varset Song8 ${InsultSong1}
		/varset Song9 ${InsultSong2}
		/varset Song10 ${DichoSong}
		/varset Song11 ${AEMezSong}
		/varset Song12 ${WarMarchSong}
		/varset Song13 ${MezSong}
	} else {
		/varset Song1 ${MainAriaSong}
		/varset Song2 ${WarMarchSong}
		/varset Song3 ${SufferingSong}
		/varset Song4 ${ArcaneSong}
		/varset Song5 ${SprySonataSong}
		/varset Song6 ${PulseRegenSong}
		/varset Song7 ${CrescendoSong}
		/varset Song8 ${InsultSong1}
		/varset Song9 ${InsultSong2}
		/varset Song10 ${DichoSong}
		/varset Song11 ${AEMezSong}
		/varset Song12 ${ChorusRegenSong}
		/varset Song13 ${MezSong}
	}
	/if (${UseIniSongs}) {
		/declare aa int local 0
		/for aa 1 to 13
			/call LoadIni SongGems Song${aa}			String "${Song${aa}}"
		/next aa
	}
/return
|----------------------------------------------------------------------------
|- SUB: SetupAura
|---------------------------------------------------------------------------- 
Sub SetupAura
	/if (${UseRegenAura}) {
		/varset BardAura ${BardRegenAura}
	} else {
		/varset BardAura ${BardDPSAura}
	}
	/if (${Me.Aura[1].ID} && ${Me.Aura[1].Name.NotEqual[${BardAura}]}) {
		/squelch /docommand ${Me.Aura[1].Remove}
	}
/return
|----------------------------------------------------------------------------
|- SUB: Bind Change Var Int resets various interger settings from ini file
|----------------------------------------------------------------------------
Sub Bind_SetVarInt(string ISection, string IName, int IVar)
    /docommand /varset changetoini 1
 |-Toggles
	/if (${ISection.Equal[Toggle]}) {
  |--Aura
		/if (${IName.Equal[UseAura]}) {
			/if (!${UseAura}) {
				/echo \aw Setting UseAura to \ag ON
				/varset UseAura 1
			} else {
				/echo \aw Resetting UseAura to \ar OFF
				/varset UseAura 0
			}
  |--Selos
		} else /if (${IName.Equal[UseAASelo]}) {
			/if (!${UseAASelo}) {
				/echo \aw Setting UseAASelo to \ag ON
				/varset UseAASelo 1
			} else {
				/echo \aw Resetting UseAASelo to \ar OFF
				/varset UseAASelo 0
			}
  |--OOC Regen
		} else /if (${IName.Equal[OOCManaSong]}) {
			/if (!${OOCManaSong}) {
				/echo \aw Setting OOCManaSong to \ag ON
				/varset OOCManaSong 1
			} else {
				/echo \aw Resetting OOCManaSong to \ar OFF
				/varset OOCManaSong 0
			}
  |--
		}
	}
/return
|----------------------------------------------------------------------------
|- SUB: BIND CmdList - 
|----------------------------------------------------------------------------
Sub Bind_CmdList
/call CommonHelp
/echo \ag===${MacroName} Commands=== 
/echo \sg/byos\aw - Bring Your Own Spells! Use what ever spells you have mem'd
/echo \ag/tglaoe\aw - Turn the use of AE abilities on/off
/echo \ag/tglbp\aw - Turn on/off the use of a Chest piece defined in the ini
/echo \ag/tglpoison\aw - Turn on/off the use of poison buffs
/echo \ag/tglmezbreak\aw - Allow attacking mez'd mobs if below assistat % - Ignores waiting for MA
/return